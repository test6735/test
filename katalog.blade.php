
@extends('layouts.app')

@section('title-block')О нас@endsection

@section('content')
<h1>Каталог</h1>

<style>

.Falder {
  color: 	#008000;
}
.Falder ~ ul li.Falder{
  color: 	#B8B8B8;
}
</style>



<ul>
    {{$temp = ''}}

    @foreach($data as $el)
        @if($el->parent_id === 0)
            {{ $temp =  $el->id }}
            <li id="{{ $el->id }}" class="Falder">{{ $el->name }}</li>
        @elseif($el->parent_id == $temp)
            {{ $temp =  $el->id }}
            <ul>
                <li id="{{ $el->id }}" class="Falder">{{ $el->name }}</li>
                @else
                    <li id="{{ $el->id }}" class="Falder">{{ $el->name }}</li>
                @endif
        @endforeach
</ul>


<script>

anonimFunction = function(e) {

  console.log(e)

  if (e.target.nextElementSibling && e.target.nextElementSibling.localName === 'ul') {
    if (e.target.nextElementSibling.style.display === 'none') {
      e.target.nextElementSibling.style.display = '';
    } else {
      e.target.nextElementSibling.style.display = 'none';
    }

  }

 };


folders =  document.getElementsByClassName('Falder');

Array.from(folders).forEach((el) => {
    el.onclick = anonimFunction;
});
</script>


@endsection


