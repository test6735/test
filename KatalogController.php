<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use App\Models\Katalog;
use Illuminate\Support\Facades\Log;


class KatalogController extends Controller {

  public function index()
  {



    $katalogs = Katalog::all();
    $sortKatalogs = [];

    foreach ($katalogs as $katalog) {
        if ($katalog->parent_id == 0) {
            $sortKatalogs[$katalog->id] = $katalog;
        } else {
            $tempArray = [];
            foreach ($sortKatalogs as $sortId => $sortKatalog) {
                $tempArray[$sortId] = $sortKatalog;
                if ($katalog->parent_id == $sortId) {
                    $tempArray[$katalog->id] = $katalog;
                } else {
                    $tempArray[$sortId] = $sortKatalog;
                }
            }
            $sortKatalogs = $tempArray;
        }
    }
     // dd($katalogs);


    return view('katalog', ['data' => $sortKatalogs]);

}

}
